const onscroll = (el, listener) => {
  el.addEventListener("scroll", listener);
};

const select = (el, all = false) => {
  el = el.trim();
  if (all) {
    return [...document.querySelectorAll(el)];
  } else {
    return document.querySelector(el);
  }
};

let selectHeader = select("#mainHeader");
if (selectHeader) {
  const headerScrolled = () => {
    if (window.scrollY > 100) {
      selectHeader.classList.add("header-scrolled");
    } else {
      selectHeader.classList.remove("header-scrolled");
    }
  };
  window.addEventListener("load", headerScrolled);
  onscroll(document, headerScrolled);
}

window.addEventListener("load", () => {
  AOS.init({
    duration: 1000,
    easing: "ease-in-out",
    once: true,
    mirror: false,
  });
});

let navbarlinks = select("#navbar .scrollto", true);
const navbarlinksActive = () => {
  let position = window.scrollY + 200;
  navbarlinks.forEach((navbarlink) => {
    if (!navbarlink.hash) return;
    let section = select(navbarlink.hash);
    if (!section) return;
    if (position >= section.offsetTop && position <= section.offsetTop + section.offsetHeight) {
      navbarlink.classList.add("active");
    } else {
      navbarlink.classList.remove("active");
    }
  });
};
window.addEventListener("load", navbarlinksActive);
onscroll(document, navbarlinksActive);

var formQuestion = document.getElementById("formQuestion");

const questionSubmitted = new bootstrap.Toast(document.getElementById("questionsubmitted"), {});
const askDokterModal = new bootstrap.Modal(document.getElementById("askDokter"), {});

formQuestion.addEventListener("submit", (e) => {
  e.preventDefault();
  console.log("HERE", questionSubmitted);
  questionSubmitted.show();
  askDokterModal.hide();
});
